<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cluster extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('ClusterModel', 'cm');
        $this->load->model('AreaModel', 'am');
        $this->tgl = $this->input->get('tanggal') ? $this->input->get('tanggal') : date('Y-m-d');
    }

    public function index()
    {
        $data['title'] = '';
        $data['namaMenu'] = '';
        $data['aktif'] = '';

        $data['tgl'] = $this->tgl;
        
        $this->load->view('cluster/cluster', $data, false);
    }

    public function getCluster()
    {
        echo $this->cm->getCluster($this->tgl);
    }

    public function getClusterID()
    {
        $id = $this->input->get('id');
        echo $this->cm->getClusterID($id);
    }

    public function addCluster()
    {
        $data['title'] = 'Add Cluster';
        $data['namaMenu'] = 'Add Cluster';
        $data['aktif'] = 'addCluster';

        //GET AREA
        $data['municipio'] = $this->am->getMunicipio();
        $data['posto'] = $this->am->getPosto();
        $data['suco'] = $this->am->getSuco();
        $data['aldeia'] = $this->am->getAldeia();

        $this->load->view('cluster/addCluster', $data, false);
    }

     public function editCluster()
    {
        // Definisi
        $id = $this->input->get('id');

        $data['title'] = 'Edit Cluster';
        $data['namaMenu'] = 'Edit Cluster';
        $data['aktif'] = 'editCluster';

        //GET AREA
        $area = (array) json_decode($this->am->getAreaClusterID($id));
        $data['allMunicipio'] = $this->am->getMunicipio();
        $data['municipio'] = $area['municipio'];
        $data['posto'] = $area['posto_adm'];
        $data['suco'] = $area['suco'];
        $data['aldeia'] = $area['aldeia'];

        $this->load->view('cluster/editCluster', $data, false);
    }

     public function addAreaCluster()
    {
        $data['title'] = 'Add Area Cluster';
        $data['namaMenu'] = 'Add Area Cluster';
        $data['aktif'] = 'addAreaCluster';

        $this->load->view('cluster/addAreaCluster', $data, false);
    }

    // PROSES

    public function prosesAddToCluster()
    {
        echo $this->cm->prosesAddToCluster();
    }

}