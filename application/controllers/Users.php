<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('UsersModel', 'tm');
        $this->tgl = $this->input->get('tanggal') ? $this->input->get('tanggal') : date('Y-m-d');
    }

    public function index()
    {
        $data['title'] = '';
        $data['namaMenu'] = '';
        $data['aktif'] = '';

        $data['tgl'] = $this->tgl;
        
        $this->load->view('users/users', $data, false);
    }

    public function getUsers()
    {
        echo $this->tm->getUsers($this->tgl);
    }

    public function getUsersID()
    {
        $id = $this->input->get('id');
        echo $this->tm->getUsersID($id);
    }

}