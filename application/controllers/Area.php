<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Area extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('ClusterModel', 'tm');
        $this->load->model('AreaModel', 'am');
        $this->tgl = $this->input->get('tanggal') ? $this->input->get('tanggal') : date('Y-m-d');
    }

    public function getPosto()
    {
      $municipio_id = $this->input->get('municipio_id');
       echo $this->am->getPosto('',$municipio_id);
    }

    public function getSuco()
    {
      $posto_adm_id = $this->input->get('posto_adm_id');
       echo $this->am->getSuco('',$posto_adm_id);
    }

    public function getAldeia()
    {
      $suco_id = $this->input->get('suco_id');
       echo $this->am->getAldeia('',$suco_id);
    }

}