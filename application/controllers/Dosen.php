<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dosen extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('LoginModel', 'lm');
    }

    public function index()
    {

        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Manager',
        ];

        $this->load->view('dosen/index', $data);
    }

    public function post_lowongan()
    {

        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Manager',
        ];

        $this->load->view('dosen/post_lowongan', $data);
    }

    public function profile_dosen()
    {
        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Manager',
        ];
        $this->load->view('dosen/profile_dosen', $data);
    }

    public function validasi_log_dosen()
    {
        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Manager',
        ];
        $this->load->view('dosen/validasi_log_dosen', $data);
    }

    public function pesan()
    {
        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Manager',
        ];
        $this->load->view('dosen/pesan', $data);
    }

    public function panduan_dosen()
    {
        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Manager',
        ];
        $this->load->view('dosen/panduan_dosen', $data);

    }

}

/* End of file Dosen.php */