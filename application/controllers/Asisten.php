<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Asisten extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('LoginModel', 'lm');
    }

    public function index()
    {

        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Asisten Dosen',
        ];

        $this->load->view('asisten/index', $data);
    }

    public function profile_asisten()
    {
        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Asisten Dosen',
        ];

        $this->load->view('asisten/profile_asisten', $data);
    }
    public function log_asisten()
    {
        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Asisten Dosen',
        ];

        $this->load->view('asisten/log_asisten', $data);
    }

    public function pesan_asisten()
    {
        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Asisten Dosen',
        ];

        $this->load->view('asisten/pesan_asisten', $data);
    }

    public function panduan_asisten()
    {
        $data = [
            'title' => 'SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Asisten Dosen',
        ];

        $this->load->view('asisten/panduan_asisten', $data);
    }
}

/* End of file Asisten.php */