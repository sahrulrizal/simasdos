<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ArtikelDepan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('ArtikelModel', 'am');
    }

    public function index()
    {
        $sidebar = json_decode($this->am->getArtikel(3));
        $artikel = json_decode($this->am->getArtikel(5));

        $data = [
            'title' => 'SIMASDOS',
            'artikel' => $artikel->response,
            'sidebar' => $sidebar->response,
        ];

        $this->load->view('login/login', $data);
    }

    public function lihatArtikel($id)
    {
        $sidebar = json_decode($this->am->getArtikel(3));
        $artikel = json_decode($this->am->getArtikelID($id));

        if ($artikel->count > 0) {
            $data = [
                'title' => $artikel->response[0]->judul,
                'artikel' => $artikel->response[0],
                'sidebar' => $sidebar->response,
            ];
            $this->load->view('artikel/lihatArtikel', $data);
        } else {
            $this->session->set_flashdata('gagal', 'Tidak dapat menemukan artikel tersebut');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

}