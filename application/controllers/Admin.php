<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('ArtikelModel', 'am');
        $this->load->model('MSupport');
        $this->load->model('ModelPesan', 'mp');
        $this->load->model('ModelPanduan', 'mopa');
        $this->load->model('Modelprofil', 'profil');
        $this->load->library('pdf');

    }

    public function index()
    {

        $data = [
            'title' => 'ADMIN SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Administrator',
            'data' => $this->am->getArtikel(),
        ];

        $this->load->view('admin/index', $data);
    }
    //in artikel
    public function inArtikel()
    {
        //deklarasi
        $FotoMenu = '';

        // Upload
        $upload = $this->MSupport->upload('FotoMenu', 1);
        $upload = json_decode($upload);
        if ($upload->error != 1) {
            $FotoMenu = str_replace(array('.jpg', '.png'), array('_thumb.jpg', '_thumb.png'), $upload->success->file_name);
        }

        $arr = array(
            'id' => $this->input->post('id'),
            'judul' => $this->input->post('judul'),
            'isi' => $this->input->post('isi'),
            'id_admin' => 1,
            'tanggal_buat' => date('Y-m-d H:i:s'),
            'status' => a,
            'gambar' => $FotoMenu,

        );
        $proses = $this->am->menuArtikel('i', '', $arr);

        if ($proses) {
            $this->session->set_flashdata('success', 'Sukses menambahkan data');
        }

        redirect($_SERVER['HTTP_REFERER']);
        // echo $proses;
    }
    //update artikel
    public function upartikel()
    {
        $FotoMenu = '';

        $upload = $this->MSupport->upload('FotoMenu', 1);
        $upload = json_decode($upload);
        if ($upload->error != 1) {
            $f = $this->input->post('h_foto');
            $f2 = str_replace(array('_thumb.jpg', '_thumb.png'), array('.jpg', '.png'), $f);
            unlink('./uploads/artikel/' . $f);
            unlink('./uploads/artikel/' . $f2);

            $FotoMenu = str_replace(array('.jpg', '.png'), array('_thumb.jpg', '_thumb.png'), $upload->success->file_name);
        } else {
            $FotoMenu = $this->input->post('h_foto');
        }

        $obj = array(
            'id' => $this->input->post('id'),
            'judul' => $this->input->post('judul'),
            'isi' => $this->input->post('isi'),
            'id_admin' => 1,
            'tanggal_buat' => date('Y-m-d H:i:s'),
            'status' => a,
            'gambar' => $FotoMenu,

        );

        $where = array(
            'id' => $this->input->post('id'),
        );

        $proses = $this->am->menuArtikel('u', '', $obj, $where);

        if ($proses) {
            $this->session->set_flashdata('success', 'Sukses Edit data');
        }

        redirect($_SERVER['HTTP_REFERER']);
        // echo $proses;
    }

    // delete artikel
    public function delartikel()
    {
        if ($this->input->get('id')) {
            $where = array(
                'id' => $this->input->get('id'),
            );

            $proses = $this->am->menuArtikel('d', '', '', $where);

            if ($proses) {
                $this->session->set_flashdata('success', 'Sukses Delete data');
            }
        }

        redirect($_SERVER['HTTP_REFERER']);
    }
    public function rekap_admin()
    {

        $data = [
            'title' => 'ADMIN SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Administrator',
        ];

        $this->load->view('admin/rekap_admin', $data);
    }

    public function jadwal_mengajar()
    {

        $data = [
            'title' => 'ADMIN SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Administrator',
        ];

        $this->load->view('admin/jadwal_mengajar', $data);
    }

    public function profile_admin()
    {
        $id = $this->session->userdata("level");
        $data = [
            'title' => 'ADMIN SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Administrator',
            'data' => $this->profil->join($id),
        ];

        // var_dump($data);
        $this->load->view('admin/profile_admin', $data);
    }
    //pesan admin
    public function pesan_admin()
    {

        $data = [
            'title' => 'ADMIN SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Administrator',
            'data' => $this->mp->menuPesan(),
        ];

        // var_dump($data);
        $this->load->view('admin/pesan_admin', $data);
    }

    // insertPesan

    public function inPesan()
    {

        $arr = array(
            'id' => $this->input->post('id'),
            'Pengirim' => $this->input->post('Pengirim'),
            'isi' => $this->input->post('isi'),

        );
        $proses = $this->mp->menuPesan('i', '', $arr);

        if ($proses) {
            $this->session->set_flashdata('success', 'Sukses menambahkan data');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    // update pesan
    public function uppesan()
    {

        $obj = array(
            'id' => $this->input->post('id'),
            'Pengirim' => $this->input->post('Pengirim'),
            'isi' => $this->input->post('isi'),
        );

        $where = array(
            'id' => $this->input->post('id'),
        );

        $proses = $this->mp->menuPesan('u', '', $obj, $where);

        if ($proses) {
            $this->session->set_flashdata('success', 'Sukses Edit data');
        }

        redirect($_SERVER['HTTP_REFERER']);
        // echo $proses;
    }

    // delete pesan
    public function delpesan()
    {
        if ($this->input->get('id')) {
            $where = array(
                'id' => $this->input->get('id'),
            );

            $proses = $this->mp->menuPesan('d', '', '', $where);

            if ($proses) {
                $this->session->set_flashdata('success', 'Sukses Delete data');
            }
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function tambah_panduan()
    {

        $data = [
            'title' => 'ADMIN SIMASDOS',
            'name_profile' => 'Richard V.Welsh',
            'level' => 'Administrator',
            'data' => $this->mopa->menupanduan(),
        ];

        // var_dump($data);

        $this->load->view('admin/tambah_panduan', $data);
    }
    // insert panduan
    public function inpanduan()
    {

        $arr = array(
            'id_panduan' => $this->input->post('id_panduan'),
            'untuk' => $this->input->post('untuk'),
            'judul' => $this->input->post('judul'),
            'isi' => $this->input->post('isi'),

        );
        $proses = $this->mopa->menupanduan('i', '', $arr);

        if ($proses) {
            $this->session->set_flashdata('success', 'Sukses menambahkan data');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    // update panduan
    public function uppanduan()
    {

        $obj = array(
            'id_panduan' => $this->input->post('id_panduan'),
            'untuk' => $this->input->post('untuk'),
            'judul' => $this->input->post('judul'),
            'isi' => $this->input->post('isi'),
        );

        $where = array(
            'id_panduan' => $this->input->post('id_panduan'),
        );

        $proses = $this->mopa->menupanduan('u', '', $obj, $where);

        if ($proses) {
            $this->session->set_flashdata('success', 'Sukses Edit data');
        }

        redirect($_SERVER['HTTP_REFERER']);
        // echo $proses;
    }

    // delete pesan
    public function delpanduan()
    {
        // id dari link gaes
        if ($this->input->get('id')) {
            $where = array(
                'id_panduan' => $this->input->get('id'),
            );

            $proses = $this->mopa->menupanduan('d', '', '', $where);

            if ($proses) {
                $this->session->set_flashdata('success', 'Sukses Delete data');
            }
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function cetak()
    {

        $pdf = new FPDF('l', 'mm', 'A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        $pdf->Cell(190, 7, 'Rekapitulasi Pembayaran', 0, 1, 'C');
        //   $pdf->SetFont('Arial','B',12);
        //   $pdf->Cell(190,7,'DAFTAR  SI SWA KELAS IX JURUSAN REKAYASA PERANGKAT LUNAK',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(20, 6, 'NIM', 1, 0);
        $pdf->Cell(85, 6, 'NAMA MA HA SISWA', 1, 0);
        $pdf->Cell(27, 6, 'NO HP', 1, 0);
        $pdf->Cell(25, 6, 'TANGGAL  L HR', 1, 1);
        $pdf->SetFont('Arial', '', 10);
        $mahasiswa = $this->db->query('select * from asdos.pembayaran')->result();
        foreach ($mahasiswa as $row) {
            $pdf->Cell(20, 6, $row->id, 1, 0);
            $pdf->Cell(85, 6, $row->id_log_mengajar, 1, 0);
            $pdf->Cell(27, 6, $row->status_bayar, 1, 0);
            $pdf->Cell(25, 6, $row->jumlah_bayar, 1, 1);
        }
        $pdf->Output();
    }

}