<?php $this->load->view('incl/main/head');?>
<?php $this->load->view('incl/main/navbar');?>


<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="col-12">
            <div class="single-blog">
                <div class="post-content text-center" style="padding: 40px;">
                    <h4 style="color: #1488CC"><b>Selamat Datang di Sistem Informasi Asisten Dosen - STT Terpadu Nurul
                            Fikri</b></h4>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-8">
            <article class="single-blog">
                <div class="post-thumb">
                    <a href="">
                        <img src="<?=base_url('template/')?>image/image.png" width="100%">
                    </a>
                </div>
                <div class="post-content text-center" style="padding: 40px">
                    <div class="post-title" style="margin: 0 auto 22px">
                        <h2><?=$artikel->judul;?></h2>
                    </div>
                    <div class="post-content">
                        <?=$artikel->isi;?>
                    </div>
                    <div class="post-footer">
                        <ul class="pull-left list-inline author-meta">
                            <li>By <a href="#"><?=$artikel->pembuat;?></a></li>
                        </ul>
                        <ul class="pull-right list-inline">
                            <li class="date"><?=$artikel->tanggal_buat;?></li>
                        </ul>
                    </div>
                </div>
            </article>
        </div>
        <div class="col-lg-4 col-sm-12">

            <?php $this->load->view('incl/main/sidebar');?>

        </div>
    </div>
</div>

<div class="container-fluid footer">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-9">
                    Copyright © 2019 Sekolah Tinggi Teknologi Terpadu Nurul Fikri. All rights reserved.

                </div>
                <div class="col-3 text-right">
                    Powered by PUSINFO STT NF
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('incl/main/script');?>

</body>

</html>