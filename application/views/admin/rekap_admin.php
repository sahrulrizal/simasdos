<?php $this->load->view('incl/backend/head');?>

<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/backend/navbar');?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/backend/sidebar');?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row purchace-popup">
                    <div class="col-12">

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <!--table laporan-->
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <h4>Rekapitulasi Pembayaran</h4>
                                    </div>
                                    <div class="col-6">
                                        <!-- <button class="btn btn-success float-right"><i class="fas fa-print"></i>
                                            Cetak</button> -->
                                        <a href="<?php echo base_url('admin/cetak'); ?>"
                                            class="btn btn-success float-right"><i class="fas fa-print"></i>
                                            Cetak</a>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Mata<br>Kuliah</th>
                                                <th>Dosen</th>
                                                <th>Ruangan</th>
                                                <th>Asisten<br>dosen</th>
                                                <th>Tanggal</th>
                                                <th>Jam<br>mulai</th>
                                                <th>Jam<br>Selesai</th>
                                                <th>Honor/jam</th>
                                                <th>Total Honor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <td>1</td>
                                            <td>Basis data</td>
                                            <td>Sahrul</td>
                                            <td>B204</td>
                                            <td>Faisal</td>
                                            <td>11/2/2019</td>
                                            <td>8.00</td>
                                            <td>11.00</td>
                                            <td>50.000</td>
                                            <td>150.000</td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--end table laporan-->
                    </div>
                </div>

                <!--Modal Mengajar-->

                <div class="modal fade" id="mengajar">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Set Jadwal Mengajar</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <form>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label>Mata Kuliah</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-3">
                                            <label>Ruangan</label>
                                            <input type="text" name="" class="form-control">
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label>Nama dosen</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-4">
                                            <label>Hari</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-3">
                                            <label>Jam mulai</label>
                                            <input type="times" name="" class="form-control">
                                        </div>
                                        <div class="col-3">
                                            <label>Jam Selesai</label>
                                            <input type="times" name="" class="form-control">
                                        </div>
                                        <div class="col-6">
                                            <label>Honor Per Jam</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success">Create</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                    </div>
                </div>

                <!--end modal mengajar-->

                <!--Modal edit Mengajar-->

                <div class="modal fade" id="editmengajar">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Jadwal Mengajar</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <form>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label>Mata Kuliah</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-3">
                                            <label>Ruangan</label>
                                            <input type="text" name="" class="form-control">
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label>Nama dosen</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-4">
                                            <label>Hari</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-3">
                                            <label>Jam mulai</label>
                                            <input type="times" name="" class="form-control">
                                        </div>
                                        <div class="col-3">
                                            <label>Jam Selesai</label>
                                            <input type="times" name="" class="form-control">
                                        </div>
                                        <div class="col-6">
                                            <label>Honor Per Jam</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success">Update</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                    </div>
                </div>

                <!--end modal edit mengajar-->

            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                        <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights
                        reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                        <i class="mdi mdi-heart text-danger"></i>
                    </span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<?php $this->load->view('incl/backend/script');?>

</body>

</html>