<?php $this->load->view('incl/backend/head'); ?>

<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/backend/navbar'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/backend/sidebar'); ?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row purchace-popup">
                    <div class="col-12">

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <!--table laporan-->
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <h4 class="">Daftar Panduan</h4>
                                    </div>
                                    <div class="col-6">
                                        <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModal">Tambah Panduan</button>
                                    </div>
                                </div>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%">Untuk</th>
                                            <th>Judul</th>
                                            <th>Isi</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        for ($i = 0; $i < json_decode($data)->count; $i++) {
                                            $row = json_decode($data)->response[$i];
                                            ?>
                                        <tr>
                                            <td><?php echo $row->untuk; ?></td>
                                            <td><?php echo $row->judul; ?></td>
                                            <td><?php echo $row->isi; ?>
                                            </td>
                                            <td>
                                                <a data-toggle="modal" data-target="#edit<?= $row->id_panduan; ?>" href="#" class="btn btn-success"> Edit</a>

                                                <a onclick="return confirm('Apakah kamu yakin ingin menghapus data ini ?')"  href="<?= site_url('/admin/delpanduan?id=' . $row->id_panduan) ?>" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                         <div class="modal fade" id="edit<?=$row->id_panduan;?>" tabindex="-1" role="dialog"
                                                aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Edit Menu Artikel</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">×</button>
                                                        </div>

                                                        <form action="<?=site_url('/admin/uppanduan')?>" method="post"
                                                            enctype="multipart/form-data">

                                                            <div class="modal-body">

                                                                <div class="form-group">
                                                                    <label>untuk</label>
                                                                    <input type="text" class="form-control" name="untuk"
                                                                        placeholder="judul" value="<?=$row->untuk;?>">
                                                                    <input type="hidden" class="form-control" name="id_panduan"
                                                                        value="<?=$row->id_panduan;?>">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>judul</label>
                                                                    <input type="text" class="form-control" name="judul"
                                                                        value="<?=$row->judul;?>">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>isi</label>
                                                                    <input type="text" class="form-control" name="isi"
                                                                        value="<?=$row->isi;?>">
                                                                </div>                                                               
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close</button>
                                                                <button type="submit"
                                                                    class="btn btn-primary">Save</button>
                                                            </div>
                                                        </form>


                                                    </div>
                                                </div>
                                            </div>
                                        <?php 
                                    }; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <!--end table laporan-->
                    </div>
                </div>

                <!--Modal-->

                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Tambah Manual</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                                <form action="<?=site_url('/admin/inpanduan')?>" method="post"
                                enctype="multipart/form-data">
                            <div class="modal-body">
                                    <div class="form-group row">
                                        <div class="col-4">
                                            <input type="text" name="id_panduan" placeholder="id panduan" class="form-control">
                                        
                                            <label>Untuk :</label>
                                            <select class="form-control" name="untuk">
                                                <option value="dosen">Dosen</option>
                                                <option value="mahasiswa">Mahasiswa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label>Judul : </label>
                                            <input type="text" name="judul" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label>Isi: </label>
                                            <textarea class="form-control" name="isi" rows="8"></textarea>
                                        </div>
                                    </div>
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">Create</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                                </form>

                        </div>
                    </div>
                </div>

                <!--end modal-->

                <!--Modal Mengajar-->

                <div class="modal fade" id="mengajar">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Set Jadwal Mengajar</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <form>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label>Mata Kuliah</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-3">
                                            <label>Ruangan</label>
                                            <input type="text" name="" class="form-control">
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label>Nama dosen</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-4">
                                            <label>Hari</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-3">
                                            <label>Jam mulai</label>
                                            <input type="times" name="" class="form-control">
                                        </div>
                                        <div class="col-3">
                                            <label>Jam Selesai</label>
                                            <input type="times" name="" class="form-control">
                                        </div>
                                        <div class="col-6">
                                            <label>Honor Per Jam</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success">Create</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                    </div>
                </div>

                <!--end modal mengajar-->

            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                        <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights
                        reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                        <i class="mdi mdi-heart text-danger"></i>
                    </span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<?php $this->load->view('incl/backend/script'); ?>

</body>

</html> 