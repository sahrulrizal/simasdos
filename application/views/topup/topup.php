<?php $this->load->view('incl/main/head'); ?>
<style type="text/css">
  
    .list-detail{
    border: solid 1px #888;
    padding: 11px;
    margin-bottom: 10px;
    border-radius: 4px;
  }

  .judul {
        border-bottom: solid 1px #888;
    margin-bottom: 10px;
    padding-bottom: 10px;
  }
</style>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/main/navbar'); ?>

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/main/sidebar'); ?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">

                          <div class="row">
                            <div class="col-md-3" style="padding-right: 0px;">

                              <div class="perhitungan" style="padding: 10px;">
                              <div class="card">
                                <div class="card-body" style="padding: 10px;border: solid 1px #000;border-radius: 4px;">
                                 <h6>Total Transaksi Berhasil</h6>
                                 <div style="padding-bottom: 2px;"><?=$totalSukses->count;?></div>
                                  <div style="font-size: 12px;color: #999;"><?=$tgl;?></div>
                                </div>
                              </div>
                              </div>
                            </div>
                            <div class="col-md-3" style="padding-left: 0px;padding-right: 0;">

                              <div class="perhitungan" style="padding: 10px;">
                              <div class="card">
                                <div class="card-body" style="padding: 10px;border: solid 1px #000;border-radius: 4px;">
                                 <h6>Total Transaksi Gagal</h6>
                                 <div style="padding-bottom: 2px;"><?=$totalGagal->count;?></div>
                                  <div style="font-size: 12px;color: #999;"><?=$tgl;?></div>
                                </div>
                              </div>
                              </div>
                            </div>
                            <div class="col-md-3" style="padding-left: 0px;padding-right: 0px;">

                              <div class="perhitungan" style="padding: 10px;">
                              <div class="card">
                                <div class="card-body" style="padding: 10px;border: solid 1px #000;border-radius: 4px;">
                                 <h6>Pendapatan Sukses</h6>
                                 <div style="padding-bottom: 2px;">$<?=$incomeSukses->response[0]->total_income == null ? 0 : $incomeSukses->response[0]->total_income;?></div>
                                  <div style="font-size: 12px;color: #999;"><?=$tgl;?></div>
                                </div>
                              </div>
                              </div>
                            </div>
                            <div class="col-md-3" style="padding-left: 0px;">

                              <div class="perhitungan" style="padding: 10px;">
                              <div class="card">
                                <div class="card-body" style="padding: 10px;border: solid 1px #000;border-radius: 4px;">
                                 <h6>Pendapatan Gagal</h6>
                                 <div style="padding-bottom: 2px;">$<?=$incomeGagal->response[0]->total_income == null ? 0 : $incomeGagal->response[0]->total_income;?></div>
                                 <div style="font-size: 12px;color: #999;"><?=$tgl;?></div>
                                </div>
                              </div>
                              </div>
                            </div>
                            <div class="col-12">
                             
                             <form>
                              <div class="form-group" style="padding: 10px;padding-bottom: 0;">
                              
                                <div class="input-group col-xs-12">
                                  <input type="date" class="form-control file-upload-info" name="tanggal" value="<?=$this->input->get('tanggal');?>" placeholder="Tanggal">
                                  <span class="input-group-append">
                                     <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Tambah Opsi
                                  </a>
                                  </span>
                                </div>
                                <div class="collapse" id="collapseExample">
                                  <div class="card card-body">
                                    <input type="text" name="nama">                                   
                                    <button style="display: none;" class="file-upload-browse btn btn-gradient-primary" type="submit">Cari</button>
                                  </div>
                                </div>

                              </div>


                              </form>

                            </div>
                          </div>
                          <hr style="margin-top: 0;">
                            <div class="card-body" style="padding-top: 0;">
                                <h4 class="card-title">Topup Pulsa</h4>
                                <div class="table-responsive">
                                    <table class="table" id="myTable">
                                      <thead>
                                        <tr>
                                          <th>#</th>
                                          <th>Oleh</th>
                                          <th>Saldo Sebelum</th>
                                          <th>Beli</th>
                                          <th>Tujuan</th>
                                          <th>Saldo Sesudah</th>
                                          <th>Status</th>
                                          <th>Tanggal</th>

                                        </tr>
                                      </thead>
                                      <tfoot>
                                        <tr>
                                          <th>#</th>
                                          <th>Oleh</th>
                                          <th>Saldo Sebelum</th>
                                          <th>Beli</th>
                                          <th>Tujuan</th>
                                          <th>Saldo Sesudah</th>
                                          <th>Status</th>
                                          <th>Tanggal</th>
                                        </tr>
                                      </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <?php $this->load->view('incl/main/footer'); ?>
            <!-- partial -->


            <!-- Modal -->
            <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Log Topup</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="list-detail">
                      <div class="judul">Oleh</div>
                      <div class="isi" id="by">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Saldo Sebelum</div>
                      <div class="isi" id="bBalance">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Beli</div>
                      <div class="isi" id="amount">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Tujuan</div>
                      <div class="isi" id="destination">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Saldo Sesudah</div>
                      <div class="isi" id="aBalance">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Status</div>
                      <div class="isi" id="status">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Tanggal</div>
                      <div class="isi" id="date">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Response</div>
                      <div class="isi" id="response">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Pesan ke Nomor Tujuan</div>
                      <div class="isi" id="msg">-</div>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<?php $this->load->view('incl/main/script'); ?>

<script type="text/javascript">
   $('#myTable').DataTable({
        // Processing indicator
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('index.php/topup/getTopup?tanggal='.$tgl); ?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
        "columnDefs": [{ 
            "targets": [0],
            "orderable": false
        }]
    });

   function detail(id="") {
    removeAtribut();
     if (id != '') {
        $.ajax({
         url: '<?php echo base_url("index.php/topup/getTopupID"); ?>',
         type: 'GET',
         dataType: 'json',
         data: {id: id},
       })
        .done(function(data) {
         console.log("success");
         getAtribut(data.response[0]);
       })
        .fail(function() {
         console.log("error");
       })
        .always(function() {
         console.log("complete");
       });
    }else{
      console.log('Tidak ada paramter ID');
    }
   }
   
   function removeAtribut() {
      $('#by').text('-');
      $('#bBalance').text('-');
      $('#amount').text('-');
      $('#destination').text('-');
      $('#aBalance').text('-');
      $('#status').text('-');
      $('#date').text('-');
      $('#response').text('-');
      $('#msg').text('-');
   }

   function getAtribut(data) {
      $('#by').text(data.by);
      $('#bBalance').text(data.bBalance);
      $('#amount').text(data.amount);
      $('#destination').text(data.destination);
      $('#aBalance').text(data.aBalance);
      $('#status').text(data.status);
      $('#date').text(data.date);
      $('#response').text(data.response);
      $('#msg').text(data.msg);
   }
   
</script>

<!-- End custom js for this page-->
</body>

</html>