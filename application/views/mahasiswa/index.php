<?php $this->load->view('incl/backend/head'); ?>

<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/backend/navbar'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/backend/sidebar_mahasiswa'); ?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row purchace-popup">
                    <div class="col-12">

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <!--table laporan-->
                        <div class="card">
                            <div class="card-body">
                                <h2>Daftar Lowongan Asisten</h2>
                                <h2 class="card-title">Genap 2018/2019</h2>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Mata Kuliah/<br>Nama Lowongan</th>
                                                <th>Prodi Mata<br>kuliah</th>
                                                <th>Semester</th>
                                                <th>Status<br>Lowongan</th>
                                                <th>Jumlah<br>Lowongan</th>
                                                <th>Jumlah<br>Pelamar</th>
                                                <th>Jumlah<br>Pelamar Diterima</th>
                                                <th>Status<br>Lamaran</th>
                                                <th>Daftar/<br>Detail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <td>1</td>
                                            <td>Basdat</td>
                                            <td>TI</td>
                                            <td>3</td>
                                            <td>Available</td>
                                            <td>4</td>
                                            <td>10</td>
                                            <td>2</td>
                                            <td>Terbuka</td>
                                            <td>
                                                <button class="btn btn-success">Daftar</button>
                                            </td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--end table laporan-->
                    </div>
                </div>

            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                        <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                        <i class="mdi mdi-heart text-danger"></i>
                    </span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<?php $this->load->view('incl/backend/script'); ?>

</body>

</html> 