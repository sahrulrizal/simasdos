<?php $this->load->view('incl/main/head'); ?>
<style type="text/css">
  
    .list-detail{
    border: solid 1px #888;
    padding: 11px;
    margin-bottom: 10px;
    border-radius: 4px;
  }

  .judul {
        border-bottom: solid 1px #888;
    margin-bottom: 10px;
    padding-bottom: 10px;
  }
</style>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/main/navbar'); ?>

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/main/sidebar'); ?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">

                            <div class="card-body" >
                                <h4 class="card-title">Users</h4>
                                <div class="table-responsive">
                                    <table class="table" id="myTable">
                                      <thead>
                                        <tr>
                                          <th>#</th>
                                          <th>Username</th>
                                          <th>Nama</th>
                                          <th>Master Number</th>
                                          <th>Position</th>
                                          <th>Saldo</th>
                                        </tr>
                                      </thead>
                                      <tfoot>
                                        <tr>
                                          <th>#</th>
                                          <th>Username</th>
                                          <th>Nama</th>
                                          <th>Master Number</th>
                                          <th>Position</th>
                                          <th>Saldo</th>
                                        </tr>
                                      </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <?php $this->load->view('incl/main/footer'); ?>
            <!-- partial -->


            <!-- Modal -->
            <div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Users</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="list-detail">
                      <div class="judul">Username</div>
                      <div class="isi" id="username">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Master Number</div>
                      <div class="isi" id="contact_number">-</div>
                    </div>

                    <div class="list-detail">
                      <div class="judul">Saldo</div>
                      <div class="isi" id="balance">-</div>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<?php $this->load->view('incl/main/script'); ?>

<script type="text/javascript">
   $('#myTable').DataTable({
        // Processing indicator
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('index.php/users/getUsers?tanggal='.$tgl); ?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
        "columnDefs": [{ 
            "targets": [0],
            "orderable": false
        }]
    });

   function detail(id="") {
    removeAtribut();
     if (id != '') {
        $.ajax({
         url: '<?php echo base_url("index.php/users/getUsersID"); ?>',
         type: 'GET',
         dataType: 'json',
         data: {id: id},
       })
        .done(function(data) {
         console.log("success");
         getAtribut(data.response[0]);
       })
        .fail(function() {
         console.log("error");
       })
        .always(function() {
         console.log("complete");
       });
    }else{
      console.log('Tidak ada paramter ID');
    }
   }
   
   function removeAtribut() {
      $('#username').text('-');
      $('#name').text('-');
      $('#contact_number').text('-');
      $('#balance').text('-');
   }

   function getAtribut(data) {
      $('#username').text(data.username);
      $('#name').text(data.name);
      $('#contact_number').text(data.contact_number);
      $('#balance').text(data.balance);
   }
   
</script>

<!-- End custom js for this page-->
</body>

</html>