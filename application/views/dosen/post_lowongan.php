<?php $this->load->view('incl/backend/head'); ?>

<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/backend/navbar'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/backend/sidebar_dosen'); ?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row purchace-popup">
                    <div class="col-12">

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <!--table laporan-->
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <h4 class="">Daftar Lowongan</h4>
                                        <br>
                                    </div>
                                    <div class="col-6">
                                        <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus"></i>Post
                                            Lowongan</button>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="10%">No</th>
                                                <th>Matakuliah</th>
                                                <th>Semester</th>
                                                <th>Jumlah<br>Lowongan</th>
                                                <th>Tanggal Posting</th>
                                                <th>Deskripsi<br>dan Persyaratan</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Matematika Dasar</td>
                                                <td>3</td>
                                                <td>4 orang</td>
                                                <td>20/2/2019</td>
                                                <td>Membantu mengajar
                                                    <br>Persyaratan : > smster 6</td>
                                                <td>
                                                    <button class="btn btn-success">Edit</button>
                                                    <button class="btn btn-danger">Delete</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <!--end table laporan-->
                    </div>
                </div>

                <!--Modal-->

                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Buat Lowongan</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <form>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label>Matakuliah</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-4">
                                            <label>Semester</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-4">
                                            <label>Jumlah Lowongan</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col4">
                                            <label>Tanggal Buat</label>
                                            <input type="date" name="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label>Dsekripsi & Syarat</label>
                                            <textarea class="form-control" rows="8"></textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success">Create</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                    </div>
                </div>

                <!--end modal-->

            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                        <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                        <i class="mdi mdi-heart text-danger"></i>
                    </span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<?php $this->load->view('incl/backend/script'); ?>

</body>

</html> 