<?php $this->load->view('incl/backend/head'); ?>

<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/backend/navbar'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/backend/sidebar_dosen'); ?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row purchace-popup">
                    <div class="col-12">

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">

                        <!--Form Cari laporan-->
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4">
                                        <h4 class="">Validasi Log Asisten</h4>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Asisten Dosen</th>
                                                <th>Mata Kuliah</th>
                                                <th>Tanggal</th>
                                                <th>Jam Mulai</th>
                                                <th>Jam Selesai</th>
                                                <th>Honor</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Sahrul Rizal</td>
                                                <td>MatDas</td>
                                                <td>11/3/2019</td>
                                                <td>10.30</td>
                                                <td>13.00</td>
                                                <td>200.000</td>
                                                <td>Waiting</td>
                                                <td>
                                                    <button class="btn btn-success">Validasi</button>
                                                    <button class="btn btn-danger">Tolak</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end form cari laporan-->
                    </div>
                </div>

            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                        <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                        <i class="mdi mdi-heart text-danger"></i>
                    </span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<?php $this->load->view('incl/backend/script'); ?>

</body>

</html> 