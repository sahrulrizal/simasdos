<?php $this->load->view('incl/main/head'); ?>
<style type="text/css">
  
    .list-detail{
    border: solid 1px #888;
    padding: 11px;
    margin-bottom: 10px;
    border-radius: 4px;
  }

  .judul {
        border-bottom: solid 1px #888;
    margin-bottom: 10px;
    padding-bottom: 10px;
  }
</style>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/main/navbar'); ?>

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/main/sidebar'); ?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">

                         <div class="card-body">
                          <h4 class="card-title">Edit Cluster</h4>
                          <p class="card-description">
                            Untuk mengedit cluster
                          </p>
                          <form class="forms-sample" action="<?=site_url('cluster/prosesEditToCluster')?>" method="get" enctype="application/x-www-form-urlencoded">
                            <div class="form-group">
                              <label for="cluster">Cluster</label>
                              <input type="text" class="form-control" name="cluster" id="cluster" placeholder="Cluster" required>
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                          
                                 <div class="form-group">
                                  <label for="exampleFormControlSelect2">Municipio</label>
                                  <select class="form-control" id="municipio" name="municipio[]" multiple="multiple">
                                    <option disabled value="">-- Silahkan Pilih --</option>
                                    <?php  for ($i=0; $i < json_decode($allMunicipio)->count; $i++) { 
                                      $row1 = json_decode($allMunicipio)->response[$i]; ?>
                                    <?php  for ($i=0; $i < $municipio->count; $i++) { 
                                      $row = $municipio->response[$i]; 
                                      if($row1->id == $row->municipio){
                                    ?>

                                      <option value="<?=$row->municipio;?>"><?=$row->name;?></option>
                                    <?php } } } ?>

                                  </select>

                                </div>

                                 <div class="form-group">
                                  <label for="exampleFormControlSelect2">Posto Adm</label>
                                  <select class="form-control" id="posto" name="posto[]" multiple="multiple">
                                    <option disabled value="">-- Silahkan Pilih --</option>
                                     
                                  </select>
                                </div>

                                 
                              </div>
                              
                              <div class="col-md-6">
                               
                               <div class="form-group">
                                  <label for="exampleFormControlSelect2">Suco</label>
                                  <select class="form-control" id="suco" name="suco[]" multiple="multiple">
                                    <option disabled value="">-- Silahkan Pilih --</option>
                                    
                                  </select>
                                </div>

                                <div class="form-group">
                                  <label for="exampleFormControlSelect2">Aldeia</label>
                                  <select class="form-control" id="aldeia" name="aldeia[]" multiple="multiple">
                                    <option disabled value="">-- Silahkan Pilih --</option>

                                  </select>
                                </div>
                              </div>
                            </div>

                              <button type="submit" class="btn btn-gradient-primary mr-2">Edit</button>
                              <button class="btn btn-light">Cancel</button>
                            </form>
                          </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <?php $this->load->view('incl/main/footer'); ?>
            <!-- partial -->


        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<?php $this->load->view('incl/main/script'); ?>

<script type="text/javascript">
 $(document).ready(function() {
    $('#municipio').select2();
    $('#posto').select2();
    $('#suco').select2();
    $('#aldeia').select2();

   // Ketika posto dipilih
  $('#municipio').change(function(event) {
    $('#posto').html('<option disabled value="">-- Silahkan Pilih --</option>');
    $.ajax({
      url: '<?=base_url();?>index.php/area/getPosto',
      type: 'GET',
      dataType: 'JSON',
      data: {municipio_id: $(this).val()},
    })
    .done(function(data) {
      var val = data.response;
      $.each( val, function( key, value ) {
       $('#posto').append('<option value="'+value.id+'">'+value.name+'</option>');
     });
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  });
  
  // Ketika posto dipilih
  $('#posto').change(function(event) {
    $('#suco').html('<option disabled value="">-- Silahkan Pilih --</option>');
    $.ajax({
      url: '<?=base_url();?>index.php/area/getSuco',
      type: 'GET',
      dataType: 'JSON',
      data: {posto_adm_id: $(this).val()},
    })
    .done(function(data) {
      var val = data.response;
      $.each( val, function( key, value ) {
       $('#suco').append('<option value="'+value.id+'">'+value.name+'</option>');
     });
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  });

  
   // Ketika suco dipilih
  $('#suco').change(function(event) {
    $('#aldeia').html('<option disabled value="">-- Silahkan Pilih --</option>');
    $.ajax({
      url: '<?=base_url();?>index.php/area/getAldeia',
      type: 'GET',
      dataType: 'JSON',
      data: {suco_id: $(this).val()},
    })
    .done(function(data) {
      var val = data.response;
      $.each( val, function( key, value ) {
       $('#aldeia').append('<option value="'+value.id+'">'+value.name+'</option>');
     });
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  }); 


});
</script>

<!-- End custom js for this page-->
</body>

</html>