 <!-- plugins:js -->
  <script src="<?=base_url('template/backend/')?>/js/vendor.bundle.base.js"></script>
  <script src="<?=base_url('template/backend/')?>/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?=base_url('template/backend/')?>js/off-canvas.js"></script>
  <script src="<?=base_url('template/backend/')?>js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?=base_url('template/backend/')?>js/dashboard.js"></script>
  <!-- End custom js for this page-->