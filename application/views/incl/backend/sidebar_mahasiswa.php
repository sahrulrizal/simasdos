<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="user-wrapper">
                    <div class="profile-image">
                        <img src="<?= base_url('template/backend/'); ?>images/faces/face4.jpg" alt="profile image">
                    </div>
                    <div class="text-wrapper">
                        <p class="profile-name"><?php echo $name_profile; ?></p>
                        <div>
                            <small class="designation text-muted"><?php echo $level; ?></small>
                            <span class="status-indicator online"></span>
                        </div>
                    </div>
                </div>
              
            </div>
        </li>


        <li class="nav-item">
            <a class="nav-link" href="lowongan.html">
                <i class="menu-icon fas fa-list-ul"></i>
                <span class="menu-title">Daftar Lowongan Asisten</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="profile.html">
                <i class="menu-icon fas fa-check"></i>
                <span class="menu-title">Pengumuman</span>
            </a>
        </li>


    </ul>
</nav> 