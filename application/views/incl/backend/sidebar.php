 <nav class="sidebar sidebar-offcanvas" id="sidebar">
     <ul class="nav">
         <li class="nav-item nav-profile">
             <div class="nav-link">
                 <div class="user-wrapper">
                     <div class="profile-image">
                         <img src="<?=base_url('template/backend/');?>images/faces/face4.jpg" alt="profile image">
                     </div>
                     <div class="text-wrapper">
                         <p class="profile-name"><?php echo $name_profile; ?></p>
                         <div>
                             <small class="designation text-muted"><?php echo $level; ?></small>
                             <span class="status-indicator online"></span>
                         </div>
                     </div>
                 </div>
                 <button class="btn btn-success btn-block" data-toggle="modal" data-target="#mengajar">Set Jadwal
                     Mengajar
                 </button>
             </div>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('admin'); ?>">
                 <i class="menu-icon fas fa-pen"></i>
                 <span class="menu-title">List/tambah Artikel</span>
             </a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('admin/rekap_admin'); ?>">
                 <i class="menu-icon fas fa-money-check-alt"></i>
                 <span class="menu-title">Rekapitulasi pembayaran</span>
             </a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('admin/jadwal_mengajar'); ?>">
                 <i class="menu-icon fas fa-calendar-week"></i>
                 <span class="menu-title">Jadwal Mengajar</span>
             </a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('admin/profile_admin'); ?>">
                 <i class="menu-icon fas fa-user"></i>
                 <span class="menu-title">Profile Admin</span>
             </a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('admin/pesan_admin'); ?>">
                 <i class="menu-icon fa  fa-envelope"></i>
                 <span class="menu-title">Pesan</span>
             </a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('admin/tambah_panduan'); ?>">
                 <i class="menu-icon fas fa-cogs"></i>
                 <span class="menu-title">Tambah Panduan</span>
             </a>
         </li>
     </ul>
 </nav>