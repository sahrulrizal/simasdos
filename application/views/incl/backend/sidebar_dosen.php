  <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
          <li class="nav-item nav-profile">
              <div class="nav-link">
                  <div class="user-wrapper">
                      <div class="profile-image">
                          <img src="<?=base_url('template/backend/');?>images/faces/face4.jpg" alt="profile image">
                      </div>
                      <div class="text-wrapper">
                          <p class="profile-name"><?php echo $name_profile; ?></p>
                          <div>
                              <small class="designation text-muted"><?php echo $level; ?></small>
                              <span class="status-indicator online"></span>
                          </div>
                      </div>
                  </div>

              </div>
          </li>

          <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('dosen/post_lowongan'); ?>">
                  <i class="menu-icon fas fa-list-ul"></i>
                  <span class="menu-title">Post Lowongan Asisten Dosen</span>
              </a>
          </li>

          <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('dosen'); ?>">
                  <i class="menu-icon fas fa-chalkboard-teacher"></i>
                  <span class="menu-title">Seleksi Asisten Dosen</span>
              </a>
          </li>

          <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('dosen/profile_dosen'); ?>">
                  <i class="menu-icon fas fa-user"></i>
                  <span class="menu-title">Profile Dosen</span>
              </a>
          </li>

          <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('dosen/validasi_log_dosen'); ?>">
                  <i class="menu-icon fas fa-file-invoice"></i>
                  <span class="menu-title">Validasi Log Asisten</span>
              </a>
          </li>

          <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('dosen/pesan'); ?>">
                  <i class="menu-icon fa  fa-envelope"></i>
                  <span class="menu-title">Pesan</span>
              </a>
          </li>

          <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('dosen/panduan_dosen'); ?>">
                  <i class="menu-icon fas fa-cogs"></i>
                  <span class="menu-title">Panduan Dosen</span>
              </a>
          </li>
      </ul>
  </nav>