 <nav class="sidebar sidebar-offcanvas" id="sidebar">
     <ul class="nav">
         <li class="nav-item nav-profile">
             <div class="nav-link">
                 <div class="user-wrapper">
                     <div class="profile-image">
                         <img src="<?=base_url('template/backend/');?>images/faces/face4.jpg" alt="profile image">
                     </div>
                     <div class="text-wrapper">
                         <p class="profile-name"><?php echo $name_profile; ?></p>
                         <div>
                             <small class="designation text-muted"><?php echo $level; ?></small>
                             <span class="status-indicator online"></span>
                         </div>
                     </div>
                 </div>

             </div>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('asisten/profile_asisten'); ?>">
                 <i class="menu-icon fas fa-user"></i>
                 <span class="menu-title">Profile Asisten</span>
             </a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('asisten/log_asisten'); ?>">
                 <i class="menu-icon fas fa-file-invoice"></i>
                 <span class="menu-title">Log Asisten Permatakuliah</span>
             </a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('asisten'); ?>">
                 <i class="menu-icon fas fa-money-bill"></i>
                 <span class="menu-title">Lihat Pembayaran Honor</span>
             </a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('asisten/pesan_asisten'); ?>">
                 <i class="menu-icon fa  fa-envelope"></i>
                 <span class="menu-title">Pesan</span>
             </a>
         </li>

         <li class="nav-item">
             <a class="nav-link" href="<?php echo base_url('asisten/panduan_asisten'); ?>">
                 <i class="menu-icon fas fa-cogs"></i>
                 <span class="menu-title">Panduan Asisten Dosen</span>
             </a>
         </li>
     </ul>
 </nav>