<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="<?=base_url('template/js/');?>sweetalert2.all.min.js"></script>
<script src="<?=base_url('template/js/');?>sweetalert2.min.js"></script>
<script src="<?=base_url('template/js/');?>jquery.sticky.js"></script>
<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<!-- Sweet Alert -->
<?php if ($this->session->flashdata('gagal')) { ?>
	<script type="text/javascript">
		Swal.fire({
			type: 'error',
			title: 'Oops...',
			text: '<?=$this->session->flashdata('gagal');?>'
		});
	</script>
<?php }else if ($this->session->flashdata('berhasil')) { ?>
	<script type="text/javascript">
		Swal.fire({
			type: 'success',
			title: 'Berhasil!',
			text: '<?=$this->session->flashdata('berhasil');?>'
		});
	</script>
<?php }else if ($this->session->flashdata('info')) { ?>
	<script type="text/javascript">
		Swal.fire({
			type: 'info',
			title: 'Info!',
			text: '<?=$this->session->flashdata('info');?>'
		})
	</script>
<?php } ?>

<script>
  $(document).ready(function(){
    $("#sticker").sticky({topSpacing:80});
  });
</script>
