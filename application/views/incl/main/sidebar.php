      <div id="sticker">


          <div class="sidebar">
              <h2 class="sidebar-title"><i class="fas fa-sign-in-alt"></i> Sign In</h2>
              <form class="form-login" method="post" action="<?=site_url('loginDepan/prosesKeLogin')?>">
                  <input type="text" name="nim_nip" placeholder="NIM / NIP" class="isian">
                  <input type="password" name="password" placeholder="Password" class="isian">
                  <input type="submit" name="login" value="Login" class="btn-login">
              </form>
          </div>

          <div class="sidebar">
              <h3 class="sidebar-title text-center">Latest Post</h3>
              <section>
                  <?php foreach ($sidebar as $v): ?>
                  <div class="row latest">
                      <div class="col-5">
                          <img src="<?=base_url('data/gambar/artikel/' . $v->gambar)?>" width="100%">
                      </div>
                      <div class="col-7">
                          <b><a href="<?=site_url('artikelDepan/lihatArtikel/' . $v->id)?>"><?=$v->judul;?></a></b>
                          <p class="date-latest"><?=$v->tanggal_buat;?></p>
                      </div>
                  </div>
                  <?php endforeach?>
              </section>
          </div>

      </div>