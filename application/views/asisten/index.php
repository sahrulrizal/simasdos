<?php $this->load->view('incl/backend/head');?>

<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/backend/navbar');?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/backend/sidebar_asisten');?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row purchace-popup">
                    <div class="col-12">

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">

                        <!--Form Cari laporan-->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="">Lihat Pembayaran Honor Per Asisten</h4>
                                <br>
                                <form class="forms-sample">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Tahun</label>
                                        <div class="col-sm-3">
                                            <input type="text" name="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Bulan</label>
                                        <div class="col-sm-3">
                                            <select class="form-control">
                                                <option>Januari</option>
                                                <option>Februari</option>
                                                <option>Maret</option>
                                                <option>April</option>
                                                <option>Mei</option>
                                                <option>Juni</option>
                                                <option>Juli</option>
                                                <option>Agustus</option>
                                                <option>september</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Status</label>
                                        <div class="col-sm-3">
                                            <select class="form-control">
                                                <option>Semua</option>
                                                <option>10</option>
                                                <option>50</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success">Lihat Laporan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- end form cari laporan-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <!--table laporan-->
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>NPM</th>
                                                <th>Asisten</th>
                                                <th>Bulan</th>
                                                <th>Mata Kuliah</th>
                                                <th>Jumlah Jam</th>
                                                <th>Honor perjam</th>
                                                <th>Jumlah Pembayaran</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <td>002</td>
                                            <td>Udin</td>
                                            <td>Juni</td>
                                            <td>Basdat</td>
                                            <td>27</td>
                                            <td>100 rb</td>
                                            <td>2700 rb</td>
                                            <td>Lunas</td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--end table laporan-->
                    </div>
                </div>

            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                        <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights
                        reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                        <i class="mdi mdi-heart text-danger"></i>
                    </span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<?php $this->load->view('incl/backend/script');?>

</body>

</html>