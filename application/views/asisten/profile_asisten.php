<?php $this->load->view('incl/backend/head');?>

<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/backend/navbar');?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/backend/sidebar_asisten');?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row purchace-popup">
                    <div class="col-12">

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">

                        <!--Form Cari laporan-->
                        <div class="card">
                            <div class="card-body">
                                <h3 class="">Profile Asisten</h3>
                                <br>



                                <form class="forms-sample">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label>Nama Asisten : </label>
                                                    <p><b>Faisal Ayash</b></p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-7">
                                                    <label>NPM</label>
                                                    <p><b>0110217009</b></p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-8">
                                                    <label>Status :</label>
                                                    <select class="form-control">
                                                        <option>Mahasiswa S1</option>
                                                        <option>Lulusan S1</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-8">
                                                    <label>Email</label>
                                                    <input type="email" name="" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label>No Hp</label>
                                                    <input type="text" name="" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-10">
                                                    <label>Waktu Kosong</label>
                                                    <textarea class="form-control" rows="4"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-2">
                                                    <label>Bank BNI : </label>
                                                </div>
                                                <div class="col-1">
                                                    <input type="checkbox" name=""
                                                        class="form-control form-check-input">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label>No rekening :</label>
                                                    <input type="text" name="" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label>Atas nama :</label>
                                                    <input type="text" name="" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <p>Pastikan entry "Nomor rekening" dan "Atas nama" sesuai dengan
                                                        nomor
                                                        rekening anda pada buku tabungan. Kesalahan pada pengisian
                                                        "Nomor rekening"
                                                        menyebabkan proses pembayaran tertunda</p>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-6">
                                                    <label>Upload Buku tabungan :</label>
                                                    <input type="file" name="" class="form-control text-right">
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <div class="col-3">

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row form-group">
                                                <div class="col-12">
                                                    <img class="float-right card"
                                                        src="<?php echo base_url('asdos/asisten/'); ?>images/profile/profile.jpg"
                                                        style="max-width: 300px; box-shadow: 0 0 5px 0 rgba(51, 51, 51, 0.08), 0 0 2px 0 rgba(51, 51, 51, 0.08); padding: 5px">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-6"></div>
                                                <div class="col-6">
                                                    <label>Upload Foto :</label>
                                                    <input type="file" name="" class="form-control text-right">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 form-group text-center">
                                            <input type="submit" name="" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>
                                </form>





                            </div>
                        </div>
                        <!-- end form cari laporan-->
                    </div>
                </div>

            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                        <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights
                        reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                        <i class="mdi mdi-heart text-danger"></i>
                    </span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<?php $this->load->view('incl/backend/script');?>

</body>

</html>