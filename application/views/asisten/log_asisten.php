<?php $this->load->view('incl/backend/head');?>

<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->load->view('incl/backend/navbar');?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <?php $this->load->view('incl/backend/sidebar_asisten');?>

        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row purchace-popup">
                    <div class="col-12">

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">

                        <!--Form Cari laporan-->
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4">
                                        <h4 class="">Log Asisten Permatakuliah</h4>
                                    </div>
                                    <div class="col-8">
                                        <button class="btn btn-success float-right" data-toggle="modal"
                                            data-target="#myModal">Buat Log</button>
                                    </div>
                                </div>
                                <br>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Mata Kuliah</th>
                                            <th>Tanggal</th>
                                            <th>Jam Mulai</th>
                                            <th>Jam Selesai</th>
                                            <th>Honor</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>MatDas</td>
                                            <td>11/3/2019</td>
                                            <td>10.30</td>
                                            <td>13.00</td>
                                            <td>200.000</td>
                                            <td>Lunas</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <!-- end form cari laporan-->
                    </div>
                </div>

                <!--Modal-->

                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Buat Log</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <form>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label>Mata Kuliah</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-6">
                                            <label>Tanggal</label>
                                            <input type="date" name="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-3">
                                            <label>Jam Mulai</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-3">
                                            <label>Jam Selesai</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                        <div class="col-4">
                                            <label>Honor</label>
                                            <input type="text" name="" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success">Create</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                    </div>
                </div>

                <!--end modal-->

            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
                        <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights
                        reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
                        <i class="mdi mdi-heart text-danger"></i>
                    </span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<?php $this->load->view('incl/backend/script');?>

</body>

</html>