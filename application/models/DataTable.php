<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DataTable extends CI_Model{
    
    function __construct() {
        $this->db2 = $this->load->database('db2', TRUE);
        // Set table name
        $this->table = '';
        // Set orderable column fields
        $this->column_order = array();
        // Set searchable column fields
        $this->column_search = array();
        // Set select column fields
        $this->select = '';
        // Set default order
        $this->order = array();
    }
    
    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData,$condition=''){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db2->limit($postData['length'], $postData['start']);
        }
        $this->db2->select($this->select);
        
        if ($condition != '') {
            for ($i=0; $i < count($condition); $i++) { 
                $this->condition($i,$condition);
            }
        }

        $query = $this->db2->get();
        return $query->result();
    }

    private function condition($i=0,$condition='')
    {
         $kondisi = $condition[$i][0];
         if ($kondisi == "join") {
             $query = $this->db2->$kondisi($condition[$i][1], $condition[$i][2], $condition[$i][3]);
         }else if($kondisi == "where" || "or_where"){
             $query = $this->db2->$kondisi($condition[$i][1], $condition[$i][2]);
         }
         return $query;
    }
    
    /*
     * Count all records
     */
    public function countAll($condition=''){
        $this->db2->from($this->table);

        if ($condition != '') {
            for ($i=0; $i < count($condition); $i++) { 
                $this->condition($i,$condition);
            }
        }

        return $this->db2->count_all_results();
    }
    
    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData,$condition=''){
        $this->_get_datatables_query($postData);
        
        if ($condition != '') {
            for ($i=0; $i < count($condition); $i++) { 
                $this->condition($i,$condition);
            }
        }

        $query = $this->db2->get();

        return $query->num_rows();
    }
    
    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData, $condition=''){
         
        $this->db2->from($this->table);
        
        if ($condition != '') {
            for ($i=0; $i < count($condition); $i++) { 
                $this->condition($i,$condition);
            }
        }

        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db2->group_start();
                    $this->db2->like($item, $postData['search']['value']);
                }else{
                    $this->db2->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db2->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db2->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db2->order_by(key($order), $order[key($order)]);
        }
    }

}