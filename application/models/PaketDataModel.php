<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaketDataModel extends CI_Model {

	 public function __construct()
    {
        parent::__construct();
    	$this->db2 = $this->load->database('db2', TRUE);
    }

    private $tabel = 'logPaketData';
    private $db = 'bonitanew';

    public function totalPaketData($status=0,$tgl='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE status = ".$status." AND date(date) = '".$tgl."'"
        ));   

        return $q;
    }

    public function totalIncomePaketData($status=0,$tgl='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT sum(amount) as total_income FROM ".$this->tabel." WHERE status = ".$status." AND date(date) = '".$tgl."'"
        ));   

        return $q;
    }

    public function getPaketData($tgl='')
    {
 		$CI =& get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'u.name','bBalance','amount','aBalance','destination',$this->tabel.'.status','date');
        // Set searchable column fields
        $CI->dt->column_search = array('u.name','destination',$this->tabel.'.date');
         // Set select column fields
        $CI->dt->select = $this->tabel.'.*,u.name';
        // Set default order
        $CI->dt->order = array($this->tabel.'.id' => 'desc');

        $data = $row = array();
        
        // Join
        $condition = array(
            array('join','users u','u.id = '.$this->tabel.'.by', 'inner'),
            array('where','date('.$this->tabel.'.date)', $tgl),
        );

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST,$condition);
        
        $i = $_POST['start'];
        foreach($dataTabel as $dt){
            $i++;
            $status = ($dt->status == 1)?'Berhasil':'Gagal';
            $data[] = array(
            	$i,
                "<a href='javascript:void(0)' onclick='detail(".$dt->id.")' data-toggle='modal' data-target='#detail'>".$dt->name."</a>",
                $dt->bBalance,
                $dt->amount,
                $dt->destination,
            	$dt->aBalance,
            	$status,
                $dt->date,
            );
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST,$condition),
            "data" => $data,
        );
        
        // Output to JSON format
        return json_encode($output);
    }

    public function getPaketDataID($id='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT l.*,l.by as id_user,u.name as 'by' FROM ".$this->tabel." l INNER JOIN users u ON u.id = l.by WHERE l.id =".$id,
        ));   

        return $q;
    }
}