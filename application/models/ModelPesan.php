<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ModelPesan extends CI_Model
{

    public function menuPesan($pilih = '', $id = '', $object = '', $arrWhere = '')
    {
        // deklarasi
        $table = 'asdos.pesan';
        $ID = 'id';
        $data = array();

        if ($pilih == 'i') {
            // jalankan query insert
            $q = $this->db->insert($table, $object);
            $data = array(
                'response' => $q,
                'msg' => 'Data Berhasil di tambahkan ke dalam tabel' . $table,
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        } else if ($pilih == 'u') {

            $q = $this->db->update($table, $object, $arrWhere);
            $data = array(
                'response' => $q,
                'msg' => 'Data Berhasil di Update Dari Table' . $table,
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        } else if ($pilih == 'd') {

            $q = $this->db->delete($table, $arrWhere);
            $data = array(
                'response' => $q,
                'msg' => 'Data Berhasil Di Delete' . $table,
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        } else {
            if ($id) {
                //jika select berdasarkan id
                $q = $this->db->get_where($table, array($ID => $id));
            } else {
                // ambil table
                $q = $this->db->get($table);
            }
            // tampung dalam array
            $data = array(
                'response' => $q->result(),
                'count' => $q->num_rows(),
                'msg' => 'Data Tersedia',
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        }

        return json_encode($data);
    }

}

/* End of file ModelName.php */

;