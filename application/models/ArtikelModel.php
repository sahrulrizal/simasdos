<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ArtikelModel extends CI_Model
{

    public $database = 'asdos';
    public $tabel = 'artikel';

    public function __construct()
    {
        parent::__construct();
        $this->tbl = $this->database . '.' . $this->tabel;
    }

    public function getArtikel($lim = 4)
    {
        $CI = &get_instance();
        $CI->load->model('SupportModel', 'sm');

        $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT a.*,ad.nama as pembuat FROM " . $this->tbl . " as a INNER JOIN " . $this->database . ".pengguna p ON a.id_admin = p.id INNER JOIN " . $this->database . ".admin ad ON ad.id_Admin = p.id LIMIT " . $lim,
        ));

        return $q;
    }

    public function getArtikelID($id = '')
    {
        $CI = &get_instance();
        $CI->load->model('SupportModel', 'sm');

        if ($id != '') {

            $q = $CI->sm->tabel(array(
                'p' => 'q',
                'q' => "SELECT a.*,ad.nama as pembuat FROM " . $this->tbl . " as a INNER JOIN " . $this->database . ".pengguna p ON a.id_admin = p.id INNER JOIN " . $this->database . ".admin ad ON ad.id_Admin = p.id WHERE a.id=" . $id,
            ));

        }

        return $q;
    }

    public function menuArtikel($pilih = '', $id = '', $object = '', $arrWhere = '')
    {
        // deklarasi
        $table = 'asdos.artikel';
        $ID = 'id';
        $data = array();

        if ($pilih == 'i') {
            // jalankan query insert
            $q = $this->db->insert($table, $object);
            $data = array(
                'response' => $q,
                'msg' => 'Data Berhasil di tambahkan ke dalam tabel' . $table,
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        } else if ($pilih == 'u') {

            $q = $this->db->update($table, $object, $arrWhere);
            $data = array(
                'response' => $q,
                'msg' => 'Data Berhasil di Update Dari Table' . $table,
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        } else if ($pilih == 'd') {

            $q = $this->db->delete($table, $arrWhere);
            $data = array(
                'response' => $q,
                'msg' => 'Data Berhasil Di Delete' . $table,
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        } else {
            if ($id) {
                //jika select berdasarkan id
                $q = $this->db->get_where($table, array($ID => $id));
            } else {
                // ambil table
                $q = $this->db->get($table);
            }
            // tampung dalam array
            $data = array(
                'response' => $q->result(),
                'count' => $q->num_rows(),
                'msg' => 'Data Tersedia',
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        }

        return json_encode($data);
    }

}