<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MSupport extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function upload($name = '', $type = '')
    {
        // Deklarasi
        $hasil = array();
        $error = 0;
        $data = '';

        switch ($type) {
            case 1:
                # kamar
                $type = "artikel";
                break;
            case 2:
                # tableuser
                $type = "tableuser";
                break;
            case 3:
                # makanan...
                $type = "transaksi";
                break;

            default:
                $type = '';
                break;
        }
        // buat folder berdasarkan type
        $config['upload_path']          = './uploads/' . $type . '/';
        $config['allowed_types']        = 'jpg|png';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($name)) {
                $error = 1;
            } else {
                $data = $this->upload->data();

                $config['image_library'] = 'gd2';
                $config['source_image'] = $data['full_path'];
                $config['create_thumb'] = true;
                $config['maintain_ratio'] = true;
                $config['width']         = 100;
                $config['height']       = 100;

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();
            }

        //hasil

        $hasil = array(
            'success' =>  $data,
            'error' => $error
        );

        return json_encode($hasil);
    }
}

/* End of file MSupport.php */


