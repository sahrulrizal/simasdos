<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends CI_Model {

	 public function __construct()
    {
        parent::__construct();
    	$this->db2 = $this->load->database('db2', TRUE);
    }

    private $tabel = 'users';
    private $db = 'bonitanew';

    public function getUsers($tgl='')
    {
 		$CI =& get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'username',$this->tabel.'.name',$this->tabel.'.contact_number','p.name','c.ballance');
        // Set searchable column fields
        $CI->dt->column_search = array('username',$this->tabel.'.name',$this->tabel.'.contact_number','p.name','c.ballance');
         // Set select column fields
        $CI->dt->select = $this->tabel.'.*,p.name as position,c.ballance as balance';
        // Set default order
        $CI->dt->order = array($this->tabel.'.id' => 'desc');

        $data = $row = array();
        
        // Join
        $condition = array(
            array('join','channel c','c.id_user = '.$this->tabel.'.id', 'inner'),
            array('join','positions p','p.id = '.$this->tabel.'.position_id', 'left'),
        );

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST,$condition);
        
        $i = $_POST['start'];
        foreach($dataTabel as $dt){
            $i++;
            $data[] = array(
            	$i,
                "<a href='javascript:void(0)' onclick='detail(".$dt->id.")' data-toggle='modal' data-target='#detail'>".$dt->username."</a>",
                $dt->name,
                $dt->contact_number,
                $dt->position,
                $dt->balance
            );
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST,$condition),
            "data" => $data,
        );
        
        // Output to JSON format
        return json_encode($output);
    }

    public function getUsersID($id='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT c.ballance as balance,u.* FROM ".$this->tabel." u INNER JOIN channel c ON u.id = c.id_user WHERE u.id =".$id,
        ));   

        return $q;
    }
}