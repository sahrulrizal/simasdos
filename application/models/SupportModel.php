<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SupportModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function tabel($arr = array())
    {
        $db = $this->db;

        // Contoh

        //  $arr = array(
        //     'pilih' => 'i,u,d,q',
        //     'database' => 'asdos',
        //     'tabel' => 'admin',
        //     'ID' => 'msisdn',
        //     'id' => '12',
        //     'where' => array(),
        //     'object' => array(),
        //     'query' => array(),
        // );

        // Deklarasi
        $pilih = @$arr['p'];
        $database = @$arr['d'];
        $tabel = @$database . '.' . @$arr['t'];
        $arrWhere = @$arr['w'];
        $object = @$arr['o'];
        $ID = @$arr['ID'];
        $id = @$arr['id'];
        $limit = @$arr['limit'];
        $order = @$arr['order'];
        $query = @$arr['q'];
        $data = array();

        if ($pilih == 'i') {

            $q = $db->insert($tabel, $object);
            $idInsert = $this->db->insert_id();
            $response = $db->get_where($tabel, $idInsert);
            $result = array(
                'first_row' => $db->get($tabel)->first_row(),
                'last_row' => $db->get($tabel)->last_row(),
                'previous_row' => $db->get($tabel)->previous_row(),
                'next_row' => $db->get($tabel)->next_row(),
            );

            $data = array(
                'request' => $object,
                'data' => $result,
                'msg' => 'Data berhasil ditambakan ke dalam tabel ' . $tabel,
                'dateTime' => date('Y-m-d H:i:s'),
                'success' => $response->num_rows() == 0 ? true : false,
            );

        } else if ($pilih == 'u') {
            $q = $db->update($tabel, $object, $arrWhere);
            $response = $db->get_where($tabel, $arrWhere);
            $request = array(
                'object' => $object,
                'where' => $arrWhere,
            );
            $result = array(
                'first_row' => $db->get($tabel)->first_row(),
                'last_row' => $db->get($tabel)->last_row(),
                'previous_row' => $db->get($tabel)->previous_row(),
                'next_row' => $db->get($tabel)->next_row(),
            );
            $data = array(
                'request' => $request,
                'response' => $response->row(),
                'data' => $result,
                'msg' => 'Data berhasil di update dari tabel ' . $tabel,
                'dateTime' => date('Y-m-d H:i:s'),
                'success' => $response->num_rows() == 1 ? true : false,
            );

        } else if ($pilih == 'd') {
            $q = $db->delete($tabel, $arrWhere);
            $response = $db->get_where($tabel, $arrWhere)->num_rows();
            $result = array(
                'first_row' => $db->get($tabel)->first_row(),
                'last_row' => $db->get($tabel)->last_row(),
                'previous_row' => $db->get($tabel)->previous_row(),
                'next_row' => $db->get($tabel)->next_row(),
            );

            $data = array(
                'request' => $arrWhere,
                'response' => $response == 0 ? "Data sudah dihapus" : "Data belum dihapus",
                'data' => $result,
                'msg' => 'Data berhasil dihapus dari tabel ' . $tabel,
                'dateTime' => date('Y-m-d H:i:s'),
                'success' => $response == 0 ? true : false,
            );

        } else if ($pilih == 'q') {
            $q = $db->query($query);

            $data = array(
                'response' => $q->result(),
                'count' => $q->num_rows(),
                'msg' => 'Data tersedia',
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );

        } else {

            if (isset($limit) || isset($order)) {
                if ($id) {
                    $q = $db->get_where($tabel, array($ID => $id), $limit, $order);
                } else {
                    $q = $db->get($tabel, $limit, $order);
                }
            } else {
                if ($id) {
                    $q = $db->get_where($tabel, array($ID => $id));
                } else {
                    $q = $db->get($tabel);
                }
            }

            $data = array(
                'response' => $q->result(),
                'count' => $q->num_rows(),
                'msg' => 'Data tersedia',
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        }

        return json_encode($data);
    }
 
    public function upload($name = '', $type = '')
    {
        // Deklarasi
        $hasil = array();
        $error = 0;
        $data = '';

        switch ($type) {
            case 1:
                # kamar
                $type = "kamar";
                break;
            case 2:
                # minuman
                $type = "minuman";
                break;
            case 3:
                # makanan...
                $type = "makanan";
                break;

            default:
                $type = '';
                break;
        }

        $config['upload_path'] = './uploads/' . $type . '/';
        $config['allowed_types'] = 'jpg|png';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($name)) {
            $error = 1;
        } else {
            $data = $this->upload->data();

            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['full_path'];
            $config['create_thumb'] = true;
            $config['maintain_ratio'] = true;
            $config['width'] = 100;
            $config['height'] = 100;

            $this->load->library('image_lib', $config);

            $this->image_lib->resize();
        }

        //hasil

        $hasil = array(
            'success' => $data,
            'error' => $error,
        );

        return json_encode($hasil);
    }

}