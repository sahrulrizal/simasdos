<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LoginModel extends CI_Model
{

    public $username;
    public $password;
    public $database = 'asdos';
    public $tabel = 'pengguna';

    public function __construct()
    {
        parent::__construct();
        $this->tbl = $this->database . '.' . $this->tabel;
    }

    public function login($username = null, $password = null)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function prosesKeLogin()
    {
        $to = '';

        $qLogin = $this->db->get_where($this->tbl, [
            'username' => $this->username,
            'password' => $this->password,
        ]);

        if ($qLogin->num_rows() > 0) {

            $q = $qLogin->row();

            $session = [
                'id' => $q->id,
                'username' => $q->username,
                'level' => $q->level,
            ];

            $this->session->set_userdata($session);

            if ($q->level == 1) {
                $this->session->set_flashdata('berhasil', 'Berhasil login sebagai Admin');
                $to = 'Admin';
            } elseif ($q->level == 2) {
                $this->session->set_flashdata('berhasil', 'Berhasil login sebagai Dosen');
                $to = 'Dosen';
            } elseif ($q->level == 3) {
                $this->session->set_flashdata('berhasil', 'Berhasil login');
                $to = 'Mahasiswa';
            } elseif ($q->level == 4) {
                $this->session->set_flashdata('berhasil', 'Berhasil login');
                $to = 'Asisten';
            } else {
                $this->session->set_flashdata('info', 'Gagal Login, dikarenakan level pengguna tidak dikenali');
                $to = '/';
            }

        } else {
            $this->session->set_flashdata('gagal', 'Gagal Login, harap periksa kembali NIM/NIP dan Password Anda.');
            $to = '/';
        }

        redirect($to);
    }
}