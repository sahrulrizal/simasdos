<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SmsModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    // report

    public function googleplay($pilih = '', $id = '', $object = '', $arrWhere = '')
    {
        $tabel = 'report1';
        $ID = 'msidn';
        $data = array();

        if ($pilih == 'i') {

            $q = $this->db->insert($tabel, $object);
            $data = array(
                'response' => $q,
                'msg' => 'Data berhasil ditambakan ke dalam tabel' . $tabel,
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );

        } else if ($pilih == 'u') {
            $q = $this->db->update($tabel, $object, $arrWhere);
            $data = array(
                'response' => $q,
                'msg' => 'Data berhasil diupdate ke dari tabel' . $tabel,
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );

        } else if ($pilih == 'd') {
            $q = $this->db->delete($tabel, $arrWhere);

            $data = array(
                'response' => $q,
                'msg' => 'Data berhasil dihapus dari tabel' . $tabel,
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );

        } else {

            if ($id) {
                $q = $this->db->get_where($tabel, array($ID => $id));
            } else {
                $q = $this->db->get($tabel);
            }

            $data = array(
                'response' => $q->result(),
                'count' => $q->num_rows(),
                'msg' => 'Data tersedia',
                'dateTime' => date('Y-m-d H:i:s'),
                'status' => true,
            );
        }

        return json_encode($data);
    }

   

}