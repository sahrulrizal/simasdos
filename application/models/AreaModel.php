<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AreaModel extends CI_Model {

	 public function __construct()
    {
        parent::__construct();
    	// $this->db2 = $this->load->database('db2', TRUE);
    }

    public function getMunicipio($id='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $this->tabel = "municipio";
        
        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
           )); 

        }else{
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel,
           ));

        }

        return $q;
    }

    public function getPosto($id='',$municipio_id='')
    {
        $this->tabel = "posto_adm";

        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');
        
        if ($id != '') {

            $q = $CI->sm->tabel(array(
                'p' => 'q',
                'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
            ));  

        }else{
          
             if ($municipio_id != '') {
                $hitung  = count($municipio_id);
                $municipio_id = implode(',', $municipio_id);
                if ($hitung > 1) {
                    $q = $CI->sm->tabel(array(
                        'p' => 'q',
                        'q' => "SELECT * FROM ".$this->tabel." WHERE municipio_id IN(".$municipio_id.")",
                    ));
                }else{
                    $q = $CI->sm->tabel(array(
                        'p' => 'q',
                        'q' => "SELECT * FROM ".$this->tabel." WHERE municipio_id=".$municipio_id,
                    ));
                }

             }else{
               
                 $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
             }
          
        }

        return $q; 
    }

    public function getSuco($id='',$posto_adm_id='')
    {
        $this->tabel = "suco";

        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');
        
        if ($id != '') {

            $q = $CI->sm->tabel(array(
                'p' => 'q',
                'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
            ));  

        }else{
          
             if ($posto_adm_id != '') {
                $hitung  = count($posto_adm_id);
                $posto_adm_id = implode(',', $posto_adm_id);
                if ($hitung > 1) {
                    $q = $CI->sm->tabel(array(
                        'p' => 'q',
                        'q' => "SELECT * FROM ".$this->tabel." WHERE posto_adm_id IN(".$posto_adm_id.")",
                    ));
                }else{
                    $q = $CI->sm->tabel(array(
                        'p' => 'q',
                        'q' => "SELECT * FROM ".$this->tabel." WHERE posto_adm_id=".$posto_adm_id,
                    ));
                }

             }else{
               
                 $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
             }
          
        }

        return $q; 
    }

    public function getAldeia($id='',$suco_id='')
    {
        $this->tabel = "aldeia";

        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');
        
        if ($id != '') {

            $q = $CI->sm->tabel(array(
                'p' => 'q',
                'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
            ));  

        }else{
          
             if ($suco_id != '') {
                $hitung  = count($suco_id);
                $suco_id = implode(',', $suco_id);
                if ($hitung > 1) {
                    $q = $CI->sm->tabel(array(
                        'p' => 'q',
                        'q' => "SELECT * FROM ".$this->tabel." WHERE suco_id IN(".$suco_id.")",
                    ));
                }else{
                    $q = $CI->sm->tabel(array(
                        'p' => 'q',
                        'q' => "SELECT * FROM ".$this->tabel." WHERE suco_id=".$suco_id,
                    ));
                }

             }else{
               
                 $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
             }
          
        }

        return $q; 
    }

    public function getAreaClusterID($id='')
    {
        $id_get = $this->input->get('id');
        if ($id == '') {
            $id = $id_get;
        }

        $arr = array();

        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $municipio = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT ar.idareacluster,name,municipio,cluster_id FROM areacluster ar INNER JOIN municipio m ON m.id = ar.municipio WHERE cluster_id = ".$id." group by municipio",
        ));

        $posto_adm = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT ar.idareacluster,name,posto_adm FROM areacluster ar INNER JOIN posto_adm p ON p.id = ar.posto_adm WHERE cluster_id = ".$id." group by posto_adm",
        ));

        $suco = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT ar.idareacluster,name,suco,cluster_id FROM areacluster ar INNER JOIN suco s ON s.id = ar.suco WHERE cluster_id = ".$id." group by suco;",
        ));

        $aldeia = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT ar.idareacluster,name,aldeia,cluster_id FROM areacluster ar INNER JOIN aldeia a ON a.id = ar.aldeia WHERE cluster_id = ".$id." group by aldeia",
        ));

        $arr = array(
            'municipio' =>  json_decode($municipio),
            'posto_adm' =>  json_decode($posto_adm),
            'suco'      =>  json_decode($suco),
            'aldeia'    =>  json_decode($aldeia),
        );

        return json_encode($arr);

    }
}